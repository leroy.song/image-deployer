#!/bin/bash
# echo "call_ansible.sh"
source ~/ansible-2.10/bin/activate 2> /dev/null
ansible --version

# #ansible --version
ANSIBLE_INVENTORY_ENABLED=ini,auto ANSIBLE_FORCE_COLOR=0 ANSIBLE_LOAD_CALLBACK_PLUGINS=1 PYTHONUNBUFFERED=1 ansible-playbook "$@"
# ANSIBLE_FORCE_COLOR=1 ANSIBLE_LOAD_CALLBACK_PLUGINS=1 PYTHONUNBUFFERED=1 ansible-playbook "$@"