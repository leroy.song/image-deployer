steps:

- task: Bash@3
  name: read_vars
  displayName: Read in Pipeline Variable Groups
  inputs:
    filePath: '$(System.DefaultWorkingDirectory)/pipelines/scripts/var-file-to-pipeline-variables.sh'
    workingDirectory: '$(System.DefaultWorkingDirectory)/pipelines/variables'
    arguments: 'common.var $(PIPELINE_SUBSCRIPTION)/$(PIPELINE_ENVIRONMENT)-environment.var $(PIPELINE_SUBSCRIPTION)/$(PIPELINE_SUBSCRIPTION)-subscription.var'

- task: Bash@3
  name: Create_SSH_necessary_files
  inputs:
    targetType: 'inline'
    script: "rm -rf ~/.ssh; mkdir ~/.ssh; touch ~/.ssh/known_hosts; echo [bitbucket.cmltd.net.au]:7999,[172.24.8.34]:7999 ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDSREu96SPREOwrI7c77VMjjHrKG4rvLUg+bHPeZ3Lg+QRXSNPkGAXIebHeK/25rEYMlA5yGuK3t13WTX+QHyUYK3YUC0iDHg0Ymu7OlUcsu1AbUV7uO6LR44H4dB2VNyAalMLB6EX2nQdHpvruItoFPvzBhjmcY+j/unGAwaMmVU5GNtp5sIEtBw/KebNu9WDy4kVEyYC6tDDQQEFyTh1DwRwJkrXQ5ofU81og5juUzNJnCEt6ryxTkouWEf9PP6OFaZFT5ERRvjWuF/8Rdfwojf4J6KOPC/M8HKNg7KLaUjyCYhda0VlymQMoQQkWXXHule66i3w+zNQPLJ6ZaXIL >> ~/.ssh/known_hosts"

- task: DownloadBuildArtifacts@0
  inputs:
    buildType: 'specific'
    project: 'Enterprise Data Platform'
    pipeline: 'edp-libraries'
    buildVersionToDownload: 'latest'
    branchName: 'master'
    downloadType: 'single'
    artifactName: 'drop' 
    downloadPath: '$(System.ArtifactsDirectory)'

- task: DownloadBuildArtifacts@0
  inputs:
    buildType: 'specific'
    project: 'AA Platform'
    pipeline: 'upload_edpDatabricks_artifact'
    buildVersionToDownload: 'latest'
    branchName: 'master'
    downloadType: 'single'
    artifactName: 'edpDatabricks' 
    downloadPath: '$(System.ArtifactsDirectory)'

- task: DownloadSecureFile@1
  name: aatestkey
  inputs:
    secureFile: 'id_rsa.aa-test'

- task: DownloadSecureFile@1
  condition: eq(variables.image_name, 'aarstudioimage')
  name: rstudiolic
  inputs:
    secureFile: '2021-06-14_RSP_Coles.lic'

- task: DownloadSecureFile@1
  condition: eq(variables.image_name, 'aarsconnectimage')
  name: rstudioconnectlic
  inputs:
    secureFile: '2021-06-14_RSC_Coles.lic'

- task: DownloadSecureFile@1
  condition: or(eq(variables.image_name, 'aarsconnectimage'), eq(variables.image_name, 'aarstudioimage'))
  name: rstudiosslcsr
  inputs:
    secureFile: 'rstudio-server.csr'

- task: DownloadSecureFile@1
  condition: or(eq(variables.image_name, 'aarsconnectimage'), eq(variables.image_name, 'aarstudioimage'))
  name: rstudiosslkey
  inputs:
    secureFile: 'rstudio-server.key'

- task: Bash@3
  displayName: Restrict permissions on key
  inputs:
    targetType: 'inline'
    script: 'chmod 600 $(aatestkey.secureFilePath)'

- task: Bash@3
  displayName: Clone modelstore2
  inputs:
    targetType: 'inline'
    script: 'ssh-agent bash -c ''ssh-add $(aatestkey.secureFilePath); git clone ssh://git@bitbucket.cmltd.net.au:7999/ade/modelstore2.git modelStore2'''
    workingDirectory: '$(System.ArtifactsDirectory)'

- task: Bash@3
  displayName: Clone aaDevOps
  inputs:
    targetType: 'inline'
    script: 'ssh-agent bash -c ''ssh-add $(aatestkey.secureFilePath); git clone ssh://git@bitbucket.cmltd.net.au:7999/ade/aadevops.git aaDevOps'''
    workingDirectory: '$(System.ArtifactsDirectory)'

- task: Bash@3
  displayName: Create tar artifact of Ansible playbooks
  inputs:
    targetType: 'inline'
    script: 'tar cvf $(System.ArtifactsDirectory)/ansible.tar Ansible'
    workingDirectory: '$(System.DefaultWorkingDirectory)'

- task: PackerTool@0
  inputs:
    version: '$(PACKER_VERSION)'

- task: Packer@1
  inputs:
    connectedServiceType: 'azure'
    azureSubscription: '$(SERVICE_CONNECTION)'
    templatePath: 'packer/image_build.json'
    command: 'build'
    variables: |
      ansible_groups=$(ansible_groups)
      image_name=$(image_name)
      ssh_key=$(aatestkey.secureFilePath)
      rstudio_lic=$(rstudiolic.secureFilePath)
      rstudio_connect_lic=$(rstudioconnectlic.secureFilePath)
      rstudio_ssl_csr=$(rstudiosslcsr.secureFilePath)
      rstudio_ssl_key=$(rstudiosslkey.secureFilePath)
      build_no=$(Build.BuildNumber)
      r_rpm_file=$(R_RPM_FILE)
      oracle_rpm_file=$(ORACLE_RPM_FILE)
      artifact_dir=$(System.ArtifactsDirectory)

      artifactory_username=$(ARTIFACTORY_USERNAME)
      artifactory_password=$(ARTIFACTORY_PASSWORD)

      subscription=$(PIPELINE_SUBSCRIPTION)
      supportgroup_tag=$(SUPPORTGROUP_TAG)
      createdby_tag=$(CREATEDBY_TAG)
      costcentre_tag=$(COSTCENTRE_TAG)
      itemindex_tag=$(ITEMINDEX_TAG)
      applicationname_tag=$(APPLICATIONNAME_TAG)
      dataclassification_tag=$(DATACLASSIFICATION_TAG)
      environment_tag=$(ENVIRONMENT_TAG)
      build_id=$(Build.BuildNumber)
      pipeline_name=$(Build.DefinitionName)

# vim:ts=2:sw=2:sts=2:et